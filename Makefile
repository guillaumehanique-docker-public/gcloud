OWNER=ghanique
IMAGENAME=gcloud

.PHONY: help
help:
	@echo "Please use any of the following targets:"
	@echo "    build        Build the dockerfile. Tag it with '${OWNER}/${IMAGENAME}'."
	@echo "    run args=    Run the image '${OWNER}/${IMAGENAME}' in a container."
	@echo "                 The args will be added to de run command.

.PHONY: build
build:
	docker build -t ${OWNER}/${IMAGENAME} -t ${OWNER}/${IMAGENAME}:144.0.0 .

.PHONY: run
run:
	docker run -ti --rm \
	-v ${CURDIR}:/wrk \
	-v ${HOME}/.config/gcloud:/root/.config/gcloud \
	${OWNER}/${IMAGENAME} ${args}
