GCloud Docker Image
===================

Introduction
------------
There is a small movement that is called "dockerize everything".
By containerizing applications Life Cycle Management becomes much easier and it's more easy to get consistent results that no longer vary depending on what machine you run it on.

This is my attempt to a contribution.
The idea is that you run `gcloud` in a container and mount the correct volumes so that it in fact executes against your machine as if it were installed locally.
(I must admit that at this point I have not yet succeeded).

Noticable Files
---------------
| File          | Description                           |
| ------------- |-------------|
| Dockerfile    | This file contains the instructions for building an image with a specific version of gcloud. It also installs the `kubectl` command. |
| gcloud        | `gcloud` is a bash script that is derived from a script that is provided by `docker` for running `docker-compose` from a container. You can put it in `/usr/bin` so that it can be invoked from anywhere. It runs the `gcloud` command in the container with the correct mappings. (Doesn't work yet). |
| Makefile      | This file contains some targets for doing basic operations like building the image without having to type (and remember) lengthy commands all the time. Execute `make` to get a list of available targets. |
