FROM ubuntu:xenial-20170119
MAINTAINER Guillaume Hanique <guillaume@hanique.com>

RUN apt-get update \
    && apt-get -y install \
        curl \ 
        python \ 
        ssh \
    && rm -rf /var/lib/apt/lists/*

ENV CLOUDSDK_COMPONENT_MANAGER_FIXED_SDK_VERSION=144.0.0 \
    CLOUDSDK_INSTALL_DIR=/usr/share
RUN curl https://sdk.cloud.google.com | bash && \
    ln -s /usr/share/google-cloud-sdk/bin/gcloud /usr/bin/gcloud
RUN [ "gcloud", "components", "install", "kubectl", "-q" ]

VOLUME /wrk
VOLUME /root/.config/gcloud

CMD [ "gcloud" ]
